#
# Makefile
#
GIT_VERSION_SHORT = $(shell git describe --abbrev=0 --dirty --always --tags 2> /dev/null)
GIT_VERSION_LONG = $(shell git describe --abbrev=6 --dirty --always --tags 2> /dev/null)
GIT_COMMIT_SHORT = $(shell git rev-parse --short -q HEAD 2> /dev/null)
ifeq ($(GIT_VERSION_SHORT),)
  GIT_VERSION_SHORT=(nogit)
  GIT_VERSION_LONG=(nogit)
  GIT_COMMIT_SHORT=(nogit)
endif

CC = gcc
CFLAGS = -Wall -Wshadow -Wundef -Wmaybe-uninitialized -fbounds-check

LVGL_DIR = ${shell pwd}

CFLAGS += -O3 -g3 -I$(LVGL_DIR)/
LDFLAGS += -lSDL2 -lm
BIN = demo


#Collect the files to compile
MAINSRC = ./main.c

include $(LVGL_DIR)/lvgl/lvgl.mk
include $(LVGL_DIR)/lv_drivers/lv_drivers.mk
include $(LVGL_DIR)/lv_examples/lv_examples.mk

CSRCS +=$(LVGL_DIR)/mouse_cursor_icon.c

OBJEXT ?= .o

AOBJS = $(ASRCS:.S=$(OBJEXT))
COBJS = $(CSRCS:.c=$(OBJEXT))

MAINOBJ = $(MAINSRC:.c=$(OBJEXT))

SRCS = $(ASRCS) $(CSRCS) $(MAINSRC)
OBJS = $(AOBJS) $(COBJS)

## MAINOBJ -> OBJFILES

all: clean default

%.o: %.c
	@$(CC)  $(CFLAGS) -c $< -o $@
	@echo "CC $<"
    
default: $(AOBJS) $(COBJS) $(MAINOBJ)
	$(CC) -o $(BIN) $(MAINOBJ) $(AOBJS) $(COBJS) $(LDFLAGS)

clean: 
	rm -f $(BIN) $(AOBJS) $(COBJS) $(MAINOBJ)

