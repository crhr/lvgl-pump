/*********************
 *      INCLUDES
 *********************/
#include "Widgets/BatteryWidget.h"

/**********************
 *  STATIC VARIABLES
 **********************/
LV_IMG_DECLARE(BATTERY_IMAGE);
LV_IMG_DECLARE(BATTERY_CHARGING_ICON);

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

/**********************
 *  STATIC VARIABLES
 **********************/

/**********************
 *  GLOBAL VARIABLES
 **********************/

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
/**
 * Create a battery object
 * @param pxParent pointer to an object, it will be the parent of the new object
 * @return pointer to the created battery object
 */
lv_obj_t * pxCreateBattery(lv_obj_t * pxParent)
{
	lv_obj_t * pxContainer = lv_cont_create(pxParent, NULL);
	lv_obj_set_auto_realign(pxContainer, true);
	lv_cont_set_fit(pxContainer, true, true);
	lv_obj_set_style(pxContainer, &lv_style_transp);

    /*Allocate the object type specific extended data*/
	BatteryExt_t * pxExt = lv_obj_allocate_ext_attr(pxContainer, sizeof(BatteryExt_t));
    lv_mem_assert(pxExt);
    if(pxExt == NULL)
    {
    	return NULL;
    }

	pxExt->xMode = BATTERY_DISCHARGING;

	// Create battery icon
	lv_obj_t * pxBatteryImg = lv_img_create(pxContainer, NULL);
	lv_img_set_src(pxBatteryImg, &BATTERY_IMAGE);
	lv_obj_set_drag(pxBatteryImg, false);

	// add progress bar using lv_obj_t
	static lv_style_t _xChargeLvlStyle;
	lv_style_copy(&_xChargeLvlStyle, &lv_style_plain_color);
	_xChargeLvlStyle.body.main_color = _xChargeLvlStyle.body.grad_color = LV_COLOR_RED;
	_xChargeLvlStyle.body.radius = BATTERY_LVL_RADIUS;

	lv_obj_t * pxChargeLvl = lv_obj_create(pxBatteryImg, NULL);
	lv_obj_set_style(pxChargeLvl, &_xChargeLvlStyle);
	lv_obj_set_size(pxChargeLvl, 0, BATTERY_IMAGE.header.h);
	lv_obj_align(pxChargeLvl, pxBatteryImg, LV_ALIGN_IN_LEFT_MID, 0, 0);
	pxExt->pxBatteryImage = pxBatteryImg;
	pxExt->pxChargeLevel = pxChargeLvl;

    // Create label with charge level
    lv_obj_t * pxPercentLabel = lv_label_create(pxBatteryImg, NULL);
    static lv_style_t _xLabelStyle;
	lv_style_t * pxTempStyle = lv_obj_get_style(pxPercentLabel);
	lv_style_copy(&_xLabelStyle, pxTempStyle);

	_xLabelStyle.text.font = &lv_font_dejavu_20;
	lv_obj_set_style(pxPercentLabel, &_xLabelStyle);
	lv_label_set_text(pxPercentLabel, "  0%");
	lv_obj_align(pxPercentLabel, pxBatteryImg, LV_ALIGN_CENTER, BATTERY_CAP / 2, 0);
	pxExt->pxContainer = pxContainer;
	pxExt->pxPercentLabel = pxPercentLabel;

	return pxContainer;
}

/**
 * Set the charge level for the battery object
 * @param pxObj pointer to a battery object
 * @param usCharge the charge level
 */
void vSetBatteryCharge(lv_obj_t * pxObj, uint16_t usCharge)
{
	if (usCharge > 100) // 100 percent
	{
		usCharge = 100;
	}
	uint16_t usNormalizedCharge = usCharge * (BATTERY_IMAGE.header.w - BATTERY_CAP) / 100;

	BatteryExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	lv_obj_t * pxCharge = pxExt->pxChargeLevel;

	lv_obj_t * pxPercentLabel = pxExt->pxPercentLabel;
	char acCharge[4];
	sprintf(acCharge, "%3d", usCharge);
	lv_label_set_text(pxPercentLabel, strcat(acCharge, "%"));

	if (lv_obj_get_width(pxCharge) == usNormalizedCharge)
	{
		return;
	}

	lv_obj_set_size(pxCharge, usNormalizedCharge, BATTERY_IMAGE.header.h);

	lv_style_t * pxTempStyle = lv_obj_get_style(pxExt->pxChargeLevel);
	uint16_t usValue = 60;
	if (usCharge <= usValue)
	{
		pxTempStyle->body.main_color = pxTempStyle->body.grad_color = LV_COLOR_MAKE(0xFF, usCharge * 0xFF / usValue, 0x00);
	}
	else
	{
		pxTempStyle->body.main_color = pxTempStyle->body.grad_color = LV_COLOR_MAKE((100 - usCharge) * 0xFF / (100 - usValue), 0xFF, 0x00);
	}
}

/**
 * Set the charge mode for the battery object
 * @param pxObj pointer to a battery object
 * @param xMode the charge mode
 */
void vSetBatteryMode(lv_obj_t * pxObj, BatteryChargeMode_t xMode)
{
	BatteryExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	if (pxExt->xMode == xMode)
	{
		return;
	}

	pxExt->xMode = xMode;
	switch (pxExt->xMode)
	{
		case BATTERY_CHARGING:
		{
			// Create image of charging sign
			lv_obj_t * pxChargingImg = lv_img_create(pxExt->pxContainer, NULL);
			lv_img_set_src(pxChargingImg, &BATTERY_CHARGING_ICON);
			lv_obj_set_drag(pxChargingImg, false);
			pxExt->pxChargingSignImage = pxChargingImg;
			lv_obj_align(pxExt->pxChargingSignImage, pxExt->pxBatteryImage, LV_ALIGN_OUT_LEFT_BOTTOM, BATTERY_GAP, 0);
			break;
		}
		case BATTERY_DISCHARGING:
		{
			lv_obj_del(pxExt->pxChargingSignImage);
			break;
		}
	}
}

/**********************
 *   STATIC FUNCTIONS
 **********************/
