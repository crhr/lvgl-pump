#ifndef BATTERYWIDGET_H_
#define BATTERYWIDGET_H_

/*********************
 *      INCLUDES
 *********************/
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include "lvgl/lvgl.h"

/*********************
 *      DEFINES
 *********************/
#ifndef BATTERY_IMAGE
#define BATTERY_IMAGE battery_icon
#endif

#ifndef BATTERY_CHARGING_ICON
#define BATTERY_CHARGING_ICON charging_sign
#endif

#ifndef BATTERY_CAP
#define BATTERY_CAP (3)
#endif

#ifndef BATTERY_LVL_RADIUS
#define BATTERY_LVL_RADIUS (1)
#endif

#ifndef BATTERY_GAP
#define BATTERY_GAP (-6)
#endif

/**********************
 *      TYPEDEFS
 **********************/
typedef enum
{
	BATTERY_DISCHARGING,
    BATTERY_CHARGING,
} BatteryChargeMode_t;

/* Data of battery */
typedef struct
{
	lv_obj_t * pxContainer;
	lv_obj_t * pxPercentLabel;
	lv_obj_t * pxChargingSignImage;
	lv_obj_t * pxBatteryImage;
	lv_obj_t * pxChargeLevel;
	BatteryChargeMode_t xMode;
} BatteryExt_t;

/**********************
 *  GLOBAL MACROS
 **********************/

/**********************
 *  GLOBAL VARIABLES
 **********************/

/**********************
 *  GLOBAL PROTOTYPES
 **********************/
/**
 * Create a battery object
 * @param pxParent pointer to an object, it will be the parent of the new object
 * @return pointer to the created battery object
 */
lv_obj_t * pxCreateBattery(lv_obj_t * pxParent);

/**
 * Set the charge level for the battery object
 * @param pxObj pointer to a battery object
 * @param usCharge the charge level
 */
void vSetBatteryCharge(lv_obj_t * pxObj, uint16_t usCharge);

/**
 * Set the charge mode for the battery object
 * @param pxObj pointer to a battery object
 * @param xMode the charge mode
 */
void vSetBatteryMode(lv_obj_t * pxObj, BatteryChargeMode_t xMode);

#endif /* BATTERYWIDGET_H_ */
