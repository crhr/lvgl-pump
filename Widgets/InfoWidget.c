/*********************
 *      INCLUDES
 *********************/
#include <stdlib.h>
#include "Widgets/InfoWidget.h"
/**********************
 *  STATIC VARIABLES
 **********************/

/*********************
 *      DEFINES
 *********************/
#define IW_SERIES_WIDTH	(2)
#define IW_WIDTH		(250)
#define IW_HEIGHT		(300)
#define IW_YMAX_DEF		(100)
#define IW_LINE_COEF	(0.9)

// colors
#define IW_COLOR_CURVE	LV_COLOR_MAKE(0x1E,0xA2,0x61)
#define IW_COLOR_BLUE	LV_COLOR_MAKE(0x26,0x4C,0x70)
#define IW_COLOR_GREEN	LV_COLOR_MAKE(0x00,0xEF,0x62)
#define IW_COLOR_TEXT	LV_COLOR_WHITE

// paddings
#define IW_MARGIN_CONT		(5)
#define IW_PAD_HLINE_TOP	(5)
#define IW_PAD_HLINE_BOTTOM	(15)
#define IW_PAD_NAME_MINUS	(26)
#define IW_PAD_NAME_DECIMAL	(13)
#define IW_PAD_NUM_RIGHT	(-5)
#define IW_PAD_FNUM_BOTTOM	(-10)
#define IW_PAD_BTW_TEXT		(2)
#define IW_PAD_BTW_TEXT_Y	(1)
#define IW_PAD_HOURS_BOTTOM	(35)

//fonts
#define IW_SMALL_TEXT	lv_font_dejavu_10
#define IW_BIG_TEXT		lv_font_dejavu_20
/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

/**********************
 *  STATIC VARIABLES
 **********************/

/**********************
 *  GLOBAL VARIABLES
 **********************/

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
/**
 * Create a info widget object
 * @param pxParent pointer to an object, it will be the parent of the new object
 * @param xSize count of points on the chart
 * @return pointer to the created object
 */
lv_obj_t * pxCreateInfoWidget(lv_obj_t * pxParent, size_t xSize)
{
	lv_obj_t * pxContainer = lv_obj_create(pxParent, NULL);
	lv_obj_set_style(pxContainer, &lv_style_transp);
	lv_obj_set_size(pxContainer, IW_WIDTH, IW_HEIGHT);

    // Allocate the object type specific extended data
	InfoExt_t * pxExt = lv_obj_allocate_ext_attr(pxContainer, sizeof(InfoExt_t));
    lv_mem_assert(pxExt);
    if(pxExt == NULL)
    {
    	return NULL;
    }

    // Create a chart
    pxExt->pxChart = lv_chart_create(pxContainer, NULL);
    lv_obj_set_style(pxExt->pxChart, &lv_style_transp);
    lv_obj_set_width(pxExt->pxChart, IW_WIDTH);
    pxExt->xYmin = 0;
    pxExt->xYmax = IW_YMAX_DEF;
	lv_chart_set_range(pxExt->pxChart, pxExt->xYmin, pxExt->xYmax);
	lv_obj_align(pxExt->pxChart, pxContainer, LV_ALIGN_IN_TOP_LEFT, 0, 0);
	lv_chart_set_type(pxExt->pxChart, LV_CHART_TYPE_LINE);
	lv_chart_set_series_opa(pxExt->pxChart, LV_OPA_100);
	lv_chart_set_series_width(pxExt->pxChart, IW_SERIES_WIDTH);
	lv_chart_set_div_line_count(pxExt->pxChart, 0, 0);
	pxExt->pxChartSeries = lv_chart_add_series(pxExt->pxChart, IW_COLOR_CURVE);
	lv_chart_set_point_count(pxExt->pxChart, 0);
	pxExt->uiPointCount = 0;
	pxExt->pxData = malloc(xSize * sizeof(lv_coord_t));
	pxExt->xDataSize = xSize;

	// Create horizontal line
	pxExt->pxPoints[0].x = 0;
	pxExt->pxPoints[0].y = 0;
	pxExt->pxPoints[1].x = lv_obj_get_width(pxExt->pxChart) * IW_LINE_COEF;
	pxExt->pxPoints[1].y = pxExt->pxPoints[0].y;
	static lv_style_t _xLineStyle;
	lv_style_copy(&_xLineStyle, &lv_style_plain);
	_xLineStyle.line.color = IW_COLOR_BLUE;
	_xLineStyle.line.width = 1;
	pxExt->pxLine = lv_line_create(pxContainer, NULL);
	lv_line_set_points(pxExt->pxLine, pxExt->pxPoints, 2);
	lv_line_set_style(pxExt->pxLine, &_xLineStyle);
	lv_obj_align(pxExt->pxLine, pxExt->pxChart, LV_ALIGN_OUT_BOTTOM_MID, 0, IW_PAD_HLINE_TOP);

	// Style for little text
	static lv_style_t _xLittleStyle;
	lv_style_copy(&_xLittleStyle, &lv_style_transp);
	_xLittleStyle.text.font = &IW_SMALL_TEXT;
	_xLittleStyle.text.color = IW_COLOR_TEXT;

	// Style for big text
	static lv_style_t _xBigStyle;
	lv_style_copy(&_xBigStyle, &lv_style_transp);
	_xBigStyle.text.font = &IW_BIG_TEXT;
	_xBigStyle.text.color = IW_COLOR_TEXT;

	// Create volume label
	pxExt->pxVolume = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxVolume, &_xBigStyle);
	pxExt->pxVolumeUnit = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxVolumeUnit, &_xLittleStyle);
	lv_label_set_text(pxExt->pxVolumeUnit, "L");
	vSetVolumeInfoWidget(pxContainer, 0.00);

	// Create hour label
	pxExt->pxHours = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHours, &_xBigStyle);
	pxExt->pxHoursUnit = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHoursUnit, &_xLittleStyle);
	lv_label_set_text(pxExt->pxHoursUnit, "h");
	vSetHoursInfoWidget(pxContainer, 0.00);

	// Create labels for Flow
	pxExt->pxFlowName = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxFlowName, &_xLittleStyle);
	lv_label_set_text(pxExt->pxFlowName, "Debit moyen");
	lv_obj_align(pxExt->pxFlowName, pxExt->pxLine, LV_ALIGN_OUT_BOTTOM_RIGHT, -IW_MARGIN_CONT, IW_PAD_HLINE_BOTTOM);
	pxExt->pxFlowInteger = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxFlowInteger, &_xBigStyle);
	pxExt->pxFlowDecimal = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxFlowDecimal, &_xLittleStyle);
	pxExt->pxFlowUnit = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxFlowUnit, &_xLittleStyle);
	lv_label_set_text(pxExt->pxFlowUnit, "L/min");
	vSetFlowInfoWidget(pxContainer, 0.00);
	pxExt->pxFlowMinus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxFlowMinus, &_xLittleStyle);
	vSetMinusFlowInfoWidget(pxContainer, 0.00);
	pxExt->pxFlowPlus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxFlowPlus, &_xLittleStyle);
	vSetPlusFlowInfoWidget(pxContainer, 0.00);

	// Create labels for Temperature
	pxExt->pxTempName = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxTempName, &_xLittleStyle);
	lv_label_set_text(pxExt->pxTempName, "Temp moyen");
	lv_obj_align(pxExt->pxTempName, pxContainer, LV_ALIGN_OUT_BOTTOM_LEFT, IW_MARGIN_CONT, 0);
	lv_obj_set_y(pxExt->pxTempName, lv_obj_get_y(pxExt->pxHours) + IW_PAD_HOURS_BOTTOM);
	pxExt->pxTempInteger = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxTempInteger, &_xBigStyle);
	pxExt->pxTempDecimal = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxTempDecimal, &_xLittleStyle);
	pxExt->pxTempUnit = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxTempUnit, &_xBigStyle);
	lv_label_set_text(pxExt->pxTempUnit, "°C");
	vSetTempInfoWidget(pxContainer, 0.00);
	pxExt->pxTempMinus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxTempMinus, &_xLittleStyle);
	vSetMinusTempInfoWidget(pxContainer, 0.0);
	pxExt->pxTempPlus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxTempPlus, &_xLittleStyle);
	vSetPlusTempInfoWidget(pxContainer, 0.0);

	// Create labels for Pressure
	pxExt->pxPressureName = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxPressureName, &_xLittleStyle);
	lv_label_set_text(pxExt->pxPressureName, "Press moyen");
	lv_obj_align(pxExt->pxPressureName, pxExt->pxLine, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
	lv_obj_set_y(pxExt->pxPressureName, lv_obj_get_y(pxExt->pxTempName));
	pxExt->pxPressureInteger = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxPressureInteger, &_xBigStyle);
	pxExt->pxPressureDecimal = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxPressureDecimal, &_xLittleStyle);
	pxExt->pxPressureUnit = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxPressureUnit, &_xBigStyle);
	lv_label_set_text(pxExt->pxPressureUnit, "Hpa");
	vSetPressureInfoWidget(pxContainer, 0.00);
	pxExt->pxPressureMinus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxPressureMinus, &_xLittleStyle);
	vSetMinusPressureInfoWidget(pxContainer, 0.0);
	pxExt->pxPressurePlus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxPressurePlus, &_xLittleStyle);
	vSetPlusPressureInfoWidget(pxContainer, 0.0);

	// Create labels for Humidity
	pxExt->pxHumidityName = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHumidityName, &_xLittleStyle);
	lv_label_set_text(pxExt->pxHumidityName, "Hydra moyen");
	lv_obj_align(pxExt->pxHumidityName, pxContainer, LV_ALIGN_OUT_BOTTOM_RIGHT, -IW_MARGIN_CONT, 0);
	lv_obj_set_y(pxExt->pxHumidityName, lv_obj_get_y(pxExt->pxPressureName));
	pxExt->pxHumidityInteger = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHumidityInteger, &_xBigStyle);
	pxExt->pxHumidityDecimal = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHumidityDecimal, &_xLittleStyle);
	pxExt->pxHumidityUnit = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHumidityUnit, &_xBigStyle);
	lv_label_set_text(pxExt->pxHumidityUnit, "%");
	vSetHumidityInfoWidget(pxContainer, 0.00);
	pxExt->pxHumidityMinus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHumidityMinus, &_xLittleStyle);
	vSetMinusHumidityInfoWidget(pxContainer, 0.0);
	pxExt->pxHumidityPlus = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHumidityPlus, &_xLittleStyle);
	vSetPlusHumidityInfoWidget(pxContainer, 0.0);

	// Create label with an experiment number
	static lv_style_t _xNumberStyle;
	lv_style_copy(&_xNumberStyle, &_xBigStyle);
	_xNumberStyle.text.color = IW_COLOR_GREEN;
	pxExt->pxHeaderNumber = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxHeaderNumber, &_xNumberStyle);
	static lv_style_t _xNumberSymbolStyle;
	lv_style_copy(&_xNumberSymbolStyle, &_xLittleStyle);
	_xNumberSymbolStyle.text.color = IW_COLOR_GREEN;
	pxExt->pxNumberSymbol = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxNumberSymbol, &_xNumberSymbolStyle);
	lv_label_set_text(pxExt->pxNumberSymbol, "N°");
	vSetHeaderNumberInfoWidget(pxContainer, 1);

	// Create footer number
	pxExt->pxFooterNumber = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxExt->pxFooterNumber, &_xBigStyle);
	vSetFooterNumberInfoWidget(pxContainer, 1, 1);

	pxExt->pxContainer = pxContainer;

	return pxExt->pxContainer;
}

/**
 * Add a point to the chart
 * @param pxObj pointer to a widget object
 * @param xData a value of a point
 */
void vAddDataInfoWidget(lv_obj_t * pxObj, lv_coord_t xData)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	uint16_t count = lv_chart_get_point_cnt(pxExt->pxChart);
	if (count >= pxExt->xDataSize)
	{
		return;
	}

	pxExt->pxData[pxExt->uiPointCount] = xData;
	lv_chart_set_point_count(pxExt->pxChart, ++pxExt->uiPointCount);
	lv_chart_set_points(pxExt->pxChart, pxExt->pxChartSeries, pxExt->pxData);
	lv_chart_refresh(pxExt->pxChart);
}

/**
 * Set a color for the curve
 * @param pxObj pointer to a widget object
 * @param xColor a new color for the curve
 */
void vSetColorInfoWidget(lv_obj_t * pxObj, lv_color_t xColor)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	pxExt->pxChartSeries->color = xColor;
}

/**
 * Set the minimal and maximal y values for the chart
 * @param pxObj pointer to a widget object
 * @param xYmin y minimum value
 * @param xYmax y maximum value
 */
void vSetRangeInfoWidget(lv_obj_t * pxObj, lv_coord_t xYmin, lv_coord_t xYmax)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	pxExt->xYmax = xYmax;
	pxExt->xYmin = xYmin;
	lv_chart_set_range(pxExt->pxChart, xYmin, xYmax);
}

/**
 * Set a value to volume text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetVolumeInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	sprintf(acValue, "%.2f", fValue);
	lv_label_set_text(pxExt->pxVolume, acValue);
	lv_obj_align(pxExt->pxVolume, pxExt->pxLine, LV_ALIGN_OUT_BOTTOM_LEFT, 0, IW_PAD_HLINE_BOTTOM);
	lv_obj_align(pxExt->pxVolumeUnit, pxExt->pxVolume, LV_ALIGN_OUT_RIGHT_BOTTOM, IW_PAD_BTW_TEXT, -IW_PAD_BTW_TEXT_Y);
}

/**
 * Set a value to hours text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetHoursInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	sprintf(acValue, "%.2f", fValue);
	lv_label_set_text(pxExt->pxHours, acValue);
	lv_obj_align(pxExt->pxHours, pxExt->pxVolume, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
	lv_obj_align(pxExt->pxHoursUnit, pxExt->pxHours, LV_ALIGN_OUT_RIGHT_BOTTOM, IW_PAD_BTW_TEXT, -IW_PAD_BTW_TEXT_Y);
}

/**
 * Set a value to flow text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetFlowInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acInt[5];
	sprintf(acInt, "%d", (int)fValue);
	lv_label_set_text(pxExt->pxFlowInteger, acInt);
	char acDecimal[4];
	int iDec = ((int)(fValue * 100)) % 100;
	if (iDec < 10)
	{
		sprintf(acDecimal, ".0%d", iDec);
	}
	else
	{
		sprintf(acDecimal, ".%d", iDec);
	}
	lv_label_set_text(pxExt->pxFlowDecimal, acDecimal);
	lv_obj_align(pxExt->pxFlowUnit, pxExt->pxFlowName, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, IW_PAD_NAME_DECIMAL);
	lv_obj_align(pxExt->pxFlowDecimal, pxExt->pxFlowUnit, LV_ALIGN_OUT_LEFT_BOTTOM, -IW_PAD_BTW_TEXT, 0);
	lv_obj_align(pxExt->pxFlowInteger, pxExt->pxFlowDecimal, LV_ALIGN_OUT_LEFT_BOTTOM, -IW_PAD_BTW_TEXT, IW_PAD_BTW_TEXT_Y);
}

/**
 * Set a value to temperature text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetTempInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	char acInt[5];
	sprintf(acInt, "%d", (int)fValue);
	lv_label_set_text(pxExt->pxTempInteger, acInt);
	char acDecimal[4];
	int iDec = ((int)(fValue * 100)) % 100;
	if (iDec < 10)
	{
		sprintf(acDecimal, ".0%d", iDec);
	}
	else
	{
		sprintf(acDecimal, ".%d", iDec);
	}
	lv_label_set_text(pxExt->pxTempDecimal, acDecimal);
	lv_obj_align(pxExt->pxTempDecimal, pxExt->pxTempName, LV_ALIGN_OUT_BOTTOM_MID, 0, IW_PAD_NAME_DECIMAL);
	lv_obj_align(pxExt->pxTempInteger, pxExt->pxTempDecimal, LV_ALIGN_OUT_LEFT_BOTTOM, -IW_PAD_BTW_TEXT, IW_PAD_BTW_TEXT_Y);
	lv_obj_align(pxExt->pxTempUnit, pxExt->pxTempDecimal, LV_ALIGN_OUT_RIGHT_BOTTOM, IW_PAD_BTW_TEXT, 0);
}

/**
 * Set a value to pressure text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetPressureInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	char acInt[5];
	sprintf(acInt, "%d", (int)fValue);
	lv_label_set_text(pxExt->pxPressureInteger, acInt);
	char acDecimal[4];
	int iDec = ((int)(fValue * 100)) % 100;
	if (iDec < 10)
	{
		sprintf(acDecimal, ".0%d", iDec);
	}
	else
	{
		sprintf(acDecimal, ".%d", iDec);
	}
	lv_label_set_text(pxExt->pxPressureDecimal, acDecimal);
	lv_obj_align(pxExt->pxPressureDecimal, pxExt->pxPressureName, LV_ALIGN_OUT_BOTTOM_MID, 0, IW_PAD_NAME_DECIMAL);
	lv_obj_align(pxExt->pxPressureInteger, pxExt->pxPressureDecimal, LV_ALIGN_OUT_LEFT_BOTTOM, -IW_PAD_BTW_TEXT, IW_PAD_BTW_TEXT_Y);
	lv_obj_align(pxExt->pxPressureUnit, pxExt->pxPressureDecimal, LV_ALIGN_OUT_RIGHT_BOTTOM, IW_PAD_BTW_TEXT, 0);
}

/**
 * Set a value to humidity text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetHumidityInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	char acInt[3];
	sprintf(acInt, "%d", (int)fValue);
	lv_label_set_text(pxExt->pxHumidityInteger, acInt);
	char acDecimal[4];
	int iDec = ((int)(fValue * 100)) % 100;
	if (iDec < 10)
	{
		sprintf(acDecimal, ".0%d", iDec);
	}
	else
	{
		sprintf(acDecimal, ".%d", iDec);
	}
	lv_label_set_text(pxExt->pxHumidityDecimal, acDecimal);
	lv_obj_align(pxExt->pxHumidityDecimal, pxExt->pxHumidityName, LV_ALIGN_OUT_BOTTOM_MID, 0, IW_PAD_NAME_DECIMAL);
	lv_obj_align(pxExt->pxHumidityInteger, pxExt->pxHumidityDecimal, LV_ALIGN_OUT_LEFT_BOTTOM, -IW_PAD_BTW_TEXT, IW_PAD_BTW_TEXT_Y);
	lv_obj_align(pxExt->pxHumidityUnit, pxExt->pxHumidityDecimal, LV_ALIGN_OUT_RIGHT_BOTTOM, IW_PAD_BTW_TEXT, 0);
}

/**
 * Set a value to minus flow text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetMinusFlowInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.2f", fValue);
	}
	else
	{
		sprintf(acValue, "%.2f", fValue);
	}

	lv_label_set_text(pxExt->pxFlowMinus, acValue);
	lv_obj_align(pxExt->pxFlowMinus, pxExt->pxFlowName, LV_ALIGN_OUT_BOTTOM_LEFT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to plus flow text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetPlusFlowInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.2f", fValue);
	}
	else
	{
		sprintf(acValue, "%.2f", fValue);
	}

	lv_label_set_text(pxExt->pxFlowPlus, acValue);
	lv_obj_align(pxExt->pxFlowPlus, pxExt->pxFlowName, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to minus temperature text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetMinusTempInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.1f", fValue);
	}
	else
	{
		sprintf(acValue, "%.1f", fValue);
	}

	lv_label_set_text(pxExt->pxTempMinus, acValue);
	lv_obj_align(pxExt->pxTempMinus, pxExt->pxTempName, LV_ALIGN_OUT_BOTTOM_LEFT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to plus temperature text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetPlusTempInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.1f", fValue);
	}
	else
	{
		sprintf(acValue, "%.1f", fValue);
	}

	lv_label_set_text(pxExt->pxTempPlus, acValue);
	lv_obj_align(pxExt->pxTempPlus, pxExt->pxTempName, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to minus pressure text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetMinusPressureInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.1f", fValue);
	}
	else
	{
		sprintf(acValue, "%.1f", fValue);
	}

	lv_label_set_text(pxExt->pxPressureMinus, acValue);
	lv_obj_align(pxExt->pxPressureMinus, pxExt->pxPressureName, LV_ALIGN_OUT_BOTTOM_LEFT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to plus pressure text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetPlusPressureInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.1f", fValue);
	}
	else
	{
		sprintf(acValue, "%.1f", fValue);
	}

	lv_label_set_text(pxExt->pxPressurePlus, acValue);
	lv_obj_align(pxExt->pxPressurePlus, pxExt->pxPressureName, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to minus humidity text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetMinusHumidityInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.1f", fValue);
	}
	else
	{
		sprintf(acValue, "%.1f", fValue);
	}

	lv_label_set_text(pxExt->pxHumidityMinus, acValue);
	lv_obj_align(pxExt->pxHumidityMinus, pxExt->pxHumidityName, LV_ALIGN_OUT_BOTTOM_LEFT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to plus humidity text label
 * @param pxObj pointer to a widget object
 * @param fValue a new value
 */
void vSetPlusHumidityInfoWidget(lv_obj_t * pxObj, float fValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	char acValue[10];
	if (fValue > 0)
	{
		sprintf(acValue, "+%.1f", fValue);
	}
	else
	{
		sprintf(acValue, "%.1f", fValue);
	}

	lv_label_set_text(pxExt->pxHumidityPlus, acValue);
	lv_obj_align(pxExt->pxHumidityPlus, pxExt->pxHumidityName, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, IW_PAD_NAME_MINUS);
}

/**
 * Set a value to header number
 * @param pxObj pointer to a widget object
 * @param usValue a new value
 */
void vSetHeaderNumberInfoWidget(lv_obj_t * pxObj, uint16_t usValue)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	char acValue[10];
	sprintf(acValue, "%d", usValue);
	lv_label_set_text(pxExt->pxHeaderNumber, acValue);
	lv_obj_align(pxExt->pxHeaderNumber, pxExt->pxChart, LV_ALIGN_IN_TOP_RIGHT, IW_PAD_NUM_RIGHT, IW_MARGIN_CONT);
	lv_obj_align(pxExt->pxNumberSymbol, pxExt->pxHeaderNumber, LV_ALIGN_OUT_LEFT_BOTTOM, -IW_PAD_BTW_TEXT, -IW_PAD_BTW_TEXT_Y);
}

/**
 * Set a value to footer number
 * @param pxObj pointer to a widget object
 * @param usNumber the first value
 * @param usAmount the second value
 */
void vSetFooterNumberInfoWidget(lv_obj_t * pxObj, uint16_t usNumber, uint16_t usAmount)
{
	InfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);
	char acText[10];
	sprintf(acText, "%d/%d", usNumber, usAmount);
	lv_label_set_text(pxExt->pxFooterNumber, acText);
	lv_obj_align(pxExt->pxFooterNumber, pxExt->pxChart, LV_ALIGN_IN_BOTTOM_RIGHT, IW_PAD_NUM_RIGHT, IW_PAD_FNUM_BOTTOM);
}
/**********************
 *   STATIC FUNCTIONS
 **********************/
