/*********************
 *      INCLUDES
 *********************/
#include <Widgets/ResumeInfoWidget.h>
/**********************
 *  STATIC VARIABLES
 **********************/

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

/**********************
 *  STATIC VARIABLES
 **********************/
static lv_style_t _xArcStyle;

/**********************
 *  GLOBAL VARIABLES
 **********************/

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
/**
 * Create a info widget
 * @param pxParent pointer to an object, it will be the parent of the new object
 * @return pointer to the created object
 */
lv_obj_t * pxCreateResumeInfo(lv_obj_t * pxParent)
{
	lv_obj_t * pxContainer = lv_obj_create(pxParent, NULL);
	lv_obj_set_style(pxContainer, &lv_style_transp);
	lv_obj_set_size(pxContainer, RESUMEINFO_CONTAINER_WIDTH, RESUMEINFO_CONTAINER_WIDTH + RESUMEINFO_CIRCLE_SIZE / 2);

	// Allocate the object type specific extended data
	ResumeInfoExt_t * pxExt = lv_obj_allocate_ext_attr(pxContainer, sizeof(ResumeInfoExt_t));
	lv_mem_assert(pxExt);
	if(pxExt == NULL)
	{
		return NULL;
	}

	// Create style for the Arc
	lv_style_copy(&_xArcStyle, &lv_style_plain);
	_xArcStyle.line.color = LV_COLOR_GREEN;
	_xArcStyle.line.width = RESUMEINFO_ARC_WIDTH;

	// Create an Arc
	lv_obj_t * pxArc = lv_arc_create(pxContainer, NULL);
	lv_arc_set_style(pxArc, LV_ARC_STYLE_MAIN, &_xArcStyle);
	lv_arc_set_angles(pxArc, 0, 360);
	lv_obj_set_size(pxArc, RESUMEINFO_CONTAINER_WIDTH, RESUMEINFO_CONTAINER_WIDTH);
	lv_obj_align(pxArc, pxContainer, LV_ALIGN_IN_TOP_MID, 0, 0);

	// Create label with value
	static lv_style_t _xPercentLabelStyle;
	lv_style_copy(&_xPercentLabelStyle, &lv_style_transp);
	_xPercentLabelStyle.text.font = &lv_font_dejavu_40;
	_xPercentLabelStyle.text.color = LV_COLOR_WHITE;
	lv_obj_t * pxPercentLabel = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxPercentLabel, &_xPercentLabelStyle);
	lv_obj_set_width(pxPercentLabel, RESUMEINFO_CONTAINER_WIDTH);
	lv_label_set_align(pxPercentLabel, LV_LABEL_ALIGN_CENTER);
	lv_label_set_text(pxPercentLabel, "");
	lv_obj_align(pxPercentLabel, pxArc, LV_ALIGN_IN_TOP_MID, 0, RESUMEINFO_PERCENT_OFFSET);

	// Create label with percent symbol
	static lv_style_t _xSymbolStyle;
	lv_style_copy(&_xSymbolStyle, &lv_style_transp);
	_xSymbolStyle.text.font = &lv_font_dejavu_20;
	_xSymbolStyle.text.color = LV_COLOR_WHITE;
	lv_obj_t * pxPercentSymbol = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxPercentSymbol, &_xSymbolStyle);
	lv_label_set_text(pxPercentSymbol, "");
	lv_obj_align(pxPercentSymbol, pxPercentLabel, LV_ALIGN_OUT_RIGHT_BOTTOM, 0, 0);

	// Create text label
	static lv_style_t _xTextStyle;
	lv_style_copy(&_xTextStyle, &lv_style_transp);
	_xTextStyle.text.font = &lv_font_dejavu_20;
	_xTextStyle.text.color = LV_COLOR_WHITE;
	lv_obj_t * pxTextLabel = lv_label_create(pxContainer, NULL);
	lv_obj_set_style(pxTextLabel, &_xTextStyle);
	lv_label_set_long_mode(pxTextLabel, LV_LABEL_LONG_BREAK);
	lv_label_set_align(pxTextLabel, LV_LABEL_ALIGN_CENTER);
	lv_obj_set_width(pxTextLabel, RESUMEINFO_CONTAINER_WIDTH);
	//lv_label_set_recolor(pxTextLabel, true);
	lv_label_set_text(pxTextLabel, "");
	lv_obj_align(pxTextLabel, pxPercentLabel, LV_ALIGN_IN_BOTTOM_MID, 0, 0);

	lv_obj_t * pxCircle = lv_obj_create(pxContainer, NULL);
	lv_obj_set_size(pxCircle, RESUMEINFO_CIRCLE_SIZE, RESUMEINFO_CIRCLE_SIZE);
	static lv_style_t _xCircleStyle;
	lv_style_copy(&_xCircleStyle, &lv_style_plain_color);
	lv_obj_set_style(pxCircle, &_xCircleStyle);
	_xCircleStyle.body.radius = LV_RADIUS_CIRCLE;
	_xCircleStyle.body.main_color = _xCircleStyle.body.grad_color = LV_COLOR_WHITE;
	lv_obj_align(pxCircle, pxArc, LV_ALIGN_IN_BOTTOM_MID, 0, RESUMEINFO_CIRCLE_SIZE / 2);

	pxExt->pxContainer = pxContainer;
	pxExt->pxPercentValue = pxPercentLabel;
	pxExt->pxPercentSymbol = pxPercentSymbol;
	pxExt->pxTextLabel = pxTextLabel;
	pxExt->pxProgressArc = pxArc;
	pxExt->pxWhiteCircle = pxCircle;
	pxExt->xState = RESUMEINFO_READY;
	vSetResumeInfoState(pxContainer, pxExt->xState, "Ready");

	return pxContainer;
}


/**
 * Set the state for the info widget object
 * @param pxObj pointer to a widget object
 * @param xState a new state
 * @param pcText text for pxTextLabel
 */
void vSetResumeInfoState(lv_obj_t * pxObj, ResumeInfoState_t xState, const char * pcText)
{
	ResumeInfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	pxExt->xState = xState;
	lv_label_set_text(pxExt->pxTextLabel, pcText);
	switch (pxExt->xState)
	{
		case RESUMEINFO_READY:
			_xArcStyle.line.color = LV_COLOR_MAKE(0x5D,0xAF,0x48); //green color
			lv_arc_set_angles(pxExt->pxProgressArc, 0, 360);
			lv_label_set_text(pxExt->pxPercentValue, "");
			lv_label_set_text(pxExt->pxPercentSymbol, "");
			lv_obj_align(pxExt->pxTextLabel, pxExt->pxProgressArc, LV_ALIGN_CENTER, 0, 0);
			break;
		case RESUMEINFO_PROBLEM:
			_xArcStyle.line.color = LV_COLOR_MAKE(0xC3,0x34,0x38); //red color
			lv_arc_set_angles(pxExt->pxProgressArc, 0, 360);
			lv_label_set_text(pxExt->pxPercentValue, "");
			lv_label_set_text(pxExt->pxPercentSymbol, "");
			lv_obj_align(pxExt->pxTextLabel, pxExt->pxProgressArc, LV_ALIGN_CENTER, 0, 0);
			break;
		case RESUMEINFO_METROLOGY_IN_PROGRESS:
			_xArcStyle.line.color = LV_COLOR_MAKE(0x7A,0xD0,0xB7); // cyan color
			lv_label_set_text(pxExt->pxPercentSymbol, "%");
			lv_obj_align(pxExt->pxPercentValue, pxExt->pxProgressArc, LV_ALIGN_IN_TOP_MID, 0, RESUMEINFO_PERCENT_OFFSET);
			lv_obj_align(pxExt->pxPercentSymbol, pxExt->pxPercentValue, LV_ALIGN_OUT_RIGHT_MID, RESUMEINFO_SYMBOL_OFFSET_X, RESUMEINFO_SYMBOL_OFFSET_Y);
			lv_obj_align(pxExt->pxTextLabel, pxExt->pxPercentValue, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
			break;
		case RESUMEINFO_WORK_IN_PROGRESS:
			_xArcStyle.line.color = LV_COLOR_MAKE(0x54,0x83,0xAF); // blue color
			lv_label_set_text(pxExt->pxPercentSymbol, "%");
			lv_obj_align(pxExt->pxPercentValue, pxExt->pxProgressArc, LV_ALIGN_IN_TOP_MID, 0, RESUMEINFO_PERCENT_OFFSET);
			lv_obj_align(pxExt->pxPercentSymbol, pxExt->pxPercentValue, LV_ALIGN_OUT_RIGHT_MID, RESUMEINFO_SYMBOL_OFFSET_X, RESUMEINFO_SYMBOL_OFFSET_Y);
			lv_obj_align(pxExt->pxTextLabel, pxExt->pxPercentValue, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
			break;
		case RESUMEINFO_WAIT:
			_xArcStyle.line.color = LV_COLOR_MAKE(0xD5,0xDE,0x54); // yellow color
			lv_arc_set_angles(pxExt->pxProgressArc, 0, 360);
			lv_label_set_text(pxExt->pxPercentValue, "");
			lv_label_set_text(pxExt->pxPercentSymbol, "");
			lv_obj_align(pxExt->pxTextLabel, pxExt->pxProgressArc, LV_ALIGN_CENTER, 0, 0);
			break;
	}

	lv_arc_set_style(pxExt->pxProgressArc, LV_ARC_STYLE_MAIN, &_xArcStyle);
}

/**
 * Set the percent value
 * @param pxObj pointer to a widget object
 * @param usPercent a new value
 */
void vSetResumeInfoPercent(lv_obj_t * pxObj, uint16_t usPercent)
{
	ResumeInfoExt_t * pxExt = lv_obj_get_ext_attr(pxObj);

	uint16_t usAngle = usPercent * 360 / 100;

	if (lv_arc_get_angle_end(pxExt->pxProgressArc) == usAngle)
	{
		return;
	}

	if (pxExt->xState == RESUMEINFO_METROLOGY_IN_PROGRESS || pxExt->xState == RESUMEINFO_WORK_IN_PROGRESS)
	{
		lv_arc_set_angles(pxExt->pxProgressArc, 0, usAngle);
		char acPercent[3];
		sprintf(acPercent, "%d", usPercent);
		lv_label_set_text(pxExt->pxPercentValue, acPercent);

		lv_obj_align(pxExt->pxPercentValue, pxExt->pxProgressArc, LV_ALIGN_IN_TOP_MID, -lv_obj_get_width(pxExt->pxPercentSymbol) / 2, RESUMEINFO_PERCENT_OFFSET);
		lv_obj_align(pxExt->pxPercentSymbol, pxExt->pxPercentValue, LV_ALIGN_OUT_RIGHT_MID, RESUMEINFO_SYMBOL_OFFSET_X, RESUMEINFO_SYMBOL_OFFSET_Y);
		lv_obj_align(pxExt->pxTextLabel, pxExt->pxPercentValue, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
	}
	else if (pxExt->xState == RESUMEINFO_WAIT)
	{
		lv_arc_set_angles(pxExt->pxProgressArc, 0, usAngle);
	}
}

/**********************
 *   STATIC FUNCTIONS
 **********************/
