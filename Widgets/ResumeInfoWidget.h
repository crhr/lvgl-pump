#ifndef RESUMEINFOWIDGET_H_
#define RESUMEINFOWIDGET_H_

/*********************
 *      INCLUDES
 *********************/
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include "lvgl/lvgl.h"

/*********************
 *      DEFINES
 *********************/
#ifndef RESUMEINFO_CONTAINER_WIDTH
#define RESUMEINFO_CONTAINER_WIDTH (150)
#endif

#ifndef RESUMEINFO_CIRCLE_SIZE
#define RESUMEINFO_CIRCLE_SIZE (30)
#endif

#ifndef RESUMEINFO_ARC_WIDTH
#define RESUMEINFO_ARC_WIDTH (8)
#endif

#ifndef RESUMEINFO_PERCENT_OFFSET
#define RESUMEINFO_PERCENT_OFFSET (35)
#endif

#ifndef RESUMEINFO_SYMBOL_OFFSET_X
#define RESUMEINFO_SYMBOL_OFFSET_X (2)
#endif

#ifndef RESUMEINFO_SYMBOL_OFFSET_Y
#define RESUMEINFO_SYMBOL_OFFSET_Y (5)
#endif

/**********************
 *      TYPEDEFS
 **********************/
typedef enum
{
	RESUMEINFO_READY,
    RESUMEINFO_PROBLEM,
	RESUMEINFO_METROLOGY_IN_PROGRESS,
	RESUMEINFO_WORK_IN_PROGRESS,
	RESUMEINFO_WAIT,
} ResumeInfoState_t;

/* Data of battery */
typedef struct
{
	lv_obj_t * pxContainer;
	lv_obj_t * pxPercentValue;
	lv_obj_t * pxPercentSymbol;
	lv_obj_t * pxTextLabel;
	lv_obj_t * pxProgressArc;
	lv_obj_t * pxWhiteCircle;
	ResumeInfoState_t xState;
} ResumeInfoExt_t;

/**********************
 *  GLOBAL MACROS
 **********************/

/**********************
 *  GLOBAL VARIABLES
 **********************/

/**********************
 *  GLOBAL PROTOTYPES
 **********************/

/**
 * Create a info widget
 * @param pxParent pointer to an object, it will be the parent of the new object
 * @return pointer to the created object
 */
lv_obj_t * pxCreateResumeInfo(lv_obj_t * pxParent);

/**
 * Set the state for the info widget object
 * @param pxObj pointer to a widget object
 * @param xState a new state
 */
void vSetResumeInfoState(lv_obj_t * pxObj, ResumeInfoState_t xState, const char * text);

/**
 * Set the percent value
 * @param pxObj pointer to a widget object
 * @param usPercent a new value
 */
void vSetResumeInfoPercent(lv_obj_t * pxObj, uint16_t usPercent);

#endif /* RESUMEINFOWIDGET_H_ */
