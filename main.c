
/**
 * @file main
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include <stdlib.h>
#include <unistd.h>
#define SDL_MAIN_HANDLED        /*To fix SDL's "undefined reference to WinMain" issue*/
#include <SDL2/SDL.h>
#include "lvgl/lvgl.h"
#include "lv_drivers/display/monitor.h"
#include "lv_drivers/indev/mouse.h"
#include "lv_drivers/indev/mousewheel.h"
#include "lv_drivers/indev/keyboard.h"
//my includes
#include "pump.h"
#include "utility.h"

/*********************
 *      DEFINES
 *********************/

/*On OSX SDL needs different handling*/
#if defined(__APPLE__) && defined(TARGET_OS_MAC)
# if __APPLE__ && TARGET_OS_MAC
#define SDL_APPLE
# endif
#endif


/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/
static void hal_init(void);
static int tick_thread(void * data);
static void memory_monitor(void * param);

static void _vCleanScreen(CoreArgs_t* pxPrvArgs);
/**********************
 *  STATIC VARIABLES
 **********************/
uint16_t _usOldCurrentWd;
uint16_t __usCurrentWd__;
static lv_style_t _xDefaultStyle;

const CoreWindowInfos_t* __axGuiWindowId__;
uint16_t __ulWindowsArraySize__;

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
void vUpdateScreen(uint16_t usWdId)
{
	__usCurrentWd__ = usWdId;
}

void vRegistrationScreen(const CoreWindowInfos_t* axGuiWindow, uint16_t usSize)
{
    __axGuiWindowId__ = axGuiWindow;
    __ulWindowsArraySize__ = usSize;
}

void vRegisterDefaultStyle(lv_style_t* pxStyle)
{
    if(!pxStyle)
    {
        return;
    }
    memcpy(&_xDefaultStyle, pxStyle, sizeof(lv_style_t));
}

int main(int argc, char ** argv)
{
    (void) argc;    /*Unused*/
    (void) argv;    /*Unused*/

    /*Initialize LittlevGL*/
    lv_init();

    /*Initialize the HAL (display, input devices, tick) for LittlevGL*/
    hal_init();

    /*Load a demo*/
//    demo_create();

    /*Try the benchmark to see how fast your GUI is*/
//    benchmark_create();

    /*Check the themes too*/
//    lv_test_theme_1(lv_theme_night_init(15, NULL));

    /* A keyboard and encoder (mouse wheel) control example*/
//    lv_test_group_1();

    CoreArgs_t pxPrvArgs =  { .usObjNumber = 0 , .apxObjects = NULL };
    void* pvArgs = NULL;

#if(BUTTON_EXTERN)
    _vInputInit();
#endif


    //Initialize the first screen
    InitScreen();


    while(1)
    {
    	//New screen will be loaded, erase the current one and create a new one.
    	_vCleanScreen(&pxPrvArgs);
    	//Create Static object for the new window
    	if(__axGuiWindowId__[__usCurrentWd__].xFnStatic)
    	{
    		__axGuiWindowId__[__usCurrentWd__].xFnStatic(&pxPrvArgs, pvArgs);
    	}
    	while(1)
    	{
    		//Update window objects
    		if(__axGuiWindowId__[__usCurrentWd__].xFnUpdate)
    		{
    			__axGuiWindowId__[__usCurrentWd__].xFnUpdate(&pxPrvArgs, pvArgs);
    		}
    		//GUI routines
    		lv_task_handler();
#if (BUTTON_EXTERN)
    		_vInputCheck(_usCurrentWd, pvArgs);
#endif
			usleep(5 * 1000);
    		if (__usCurrentWd__  != _usOldCurrentWd)
    		{
    			_usOldCurrentWd = __usCurrentWd__;
    			break; //New screen, leave the current one
    		}
    	}
    }

#ifdef SDL_APPLE
    SDL_Event event;

    while(SDL_PollEvent(&event)) {
#if USE_MOUSE != 0
    	mouse_handler(&event);
#endif

#if USE_KEYBOARD
    	keyboard_handler(&event);
#endif

#if USE_MOUSEWHEEL != 0
    	mousewheel_handler(&event);
#endif
    }
#endif


    return 0;
}

/**********************
 *   STATIC FUNCTIONS
 **********************/
static void _vCleanScreen(CoreArgs_t* pxPrvArgs)
{
    static lv_obj_t * pxScr = NULL; //screen handle
    //Delete screen container
    if(pxPrvArgs->apxObjects)
    {
        free((void*)(pxPrvArgs->apxObjects)); //private args use dynamic memory
        pxPrvArgs->apxObjects = NULL;
    }
    //Delete screen    //Prepare a new screen
    if(pxScr)
    {
        lv_obj_clean(pxScr);
        pxScr = NULL ;
    }
    //Prepare a new screen
    pxScr = lv_obj_create(NULL, NULL);
    lv_obj_set_style(pxScr, &_xDefaultStyle);
    lv_scr_load(pxScr);
}

/**
 * Initialize the Hardware Abstraction Layer (HAL) for the Littlev graphics library
 */
static void hal_init(void)
{
    /* Add a display
     * Use the 'monitor' driver which creates window on PC's monitor to simulate a display*/
    monitor_init();
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);            /*Basic initialization*/
    disp_drv.disp_flush = monitor_flush;    /*Used when `LV_VDB_SIZE != 0` in lv_conf.h (buffered drawing)*/
    disp_drv.disp_fill = monitor_fill;      /*Used when `LV_VDB_SIZE == 0` in lv_conf.h (unbuffered drawing)*/
    disp_drv.disp_map = monitor_map;        /*Used when `LV_VDB_SIZE == 0` in lv_conf.h (unbuffered drawing)*/
    lv_disp_drv_register(&disp_drv);

    /* Add the mouse as input device
     * Use the 'mouse' driver which reads the PC's mouse*/
    mouse_init();
    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);          /*Basic initialization*/
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read = mouse_read;         /*This function will be called periodically (by the library) to get the mouse position and state*/
    lv_indev_t * mouse_indev = lv_indev_drv_register(&indev_drv);

    /*Set a cursor for the mouse*/
    LV_IMG_DECLARE(mouse_cursor_icon);                          /*Declare the image file.*/
    lv_obj_t * cursor_obj =  lv_img_create(lv_scr_act(), NULL); /*Create an image object for the cursor */
    lv_img_set_src(cursor_obj, &mouse_cursor_icon);             /*Set the image source*/
    lv_indev_set_cursor(mouse_indev, cursor_obj);               /*Connect the image  object to the driver*/

    /* Tick init.
     * You have to call 'lv_tick_inc()' in periodically to inform LittelvGL about how much time were elapsed
     * Create an SDL thread to do this*/
    SDL_CreateThread(tick_thread, "tick", NULL);

    /* Optional:
     * Create a memory monitor task which prints the memory usage in periodically.*/
    lv_task_create(memory_monitor, 3000, LV_TASK_PRIO_MID, NULL);
}

/**
 * A task to measure the elapsed time for LittlevGL
 * @param data unused
 * @return never return
 */
static int tick_thread(void * data)
{
    (void)data;

    while(1) {
        SDL_Delay(5);   /*Sleep for 5 millisecond*/
        lv_tick_inc(5); /*Tell LittelvGL that 5 milliseconds were elapsed*/
    }

    return 0;
}

/**
 * Print the memory usage periodically
 * @param param
 */
static void memory_monitor(void * param)
{
    (void) param; /*Unused*/

    lv_mem_monitor_t mon;
    lv_mem_monitor(&mon);
    printf("used: %6d (%3d %%), frag: %3d %%, biggest free: %6d\n", (int)mon.total_size - mon.free_size,
           mon.used_pct,
           mon.frag_pct,
           (int)mon.free_biggest_size);
}
