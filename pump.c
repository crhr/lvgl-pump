/** @copyright Copyright (C) C&S Company, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 * @author    MARAZANO Lilian (l.marazano@cs-equipments.com)
 * @date      2019
 */

/*********************
 *      INCLUDES
 *********************/
#include "pump.h"
#include "utility.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include "Widgets/InfoWidget.h"
#include "Widgets/BatteryWidget.h"
#include "Widgets/ResumeInfoWidget.h"

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/
static int _iScreenIcon(CoreArgs_t* pxPrvArgs, void* pvArgs);
static int _iUpdateLogo(CoreArgs_t* pxPrvArgs, void* pvArgs);
/**********************
 *  STATIC VARIABLES
 **********************/
/* declare a table of function pointers */
static const CoreWindowInfos_t _axGuiWindowId[] = {
    STATE_TABLE(coreGENERATE_WIN_ARRAY)
};
static lv_obj_t * _pxBattery;
static lv_obj_t * _pxInfoWidget;
/**********************
 *  GLOBAL VARIABLES
 **********************/

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/
void InitScreen(void)
{
    //Register array
    vRegistrationScreen(_axGuiWindowId, sizeof(_axGuiWindowId)/sizeof(CoreWindowInfos_t));

    //default style
    lv_style_t xStyleLvgl;
    lv_style_copy(&xStyleLvgl, &lv_style_plain);
//    xStyleLvgl.text.font = &terminus_h12_1bpp;   /*Unicode and symbol fonts already assigned by the library*/
//    xStyleLvgl.text.color.full = 1;
//    xStyleLvgl.text.opa = 255;
//
//    xStyleLvgl.image.color.full = 1;
//    xStyleLvgl.image.intense = 255;
//    xStyleLvgl.image.opa = 255;
//
//    xStyleLvgl.line.color.full = 1;
//    xStyleLvgl.line.opa = 255;
//    xStyleLvgl.line.width = 1;
//    xStyleLvgl.line.rounded = false;
//
//    xStyleLvgl.body.main_color.full = 0;
//    xStyleLvgl.body.grad_color.full = 0;
//    xStyleLvgl.body.shadow.color.full = 0;
//    xStyleLvgl.body.border.color.full = 0;
//    xStyleLvgl.body.empty = 0;

//    xStyleLvgl.body.main_color = LV_COLOR_MAKE(0x56,0x64,0x8C);
//    xStyleLvgl.body.grad_color = LV_COLOR_MAKE(0x1D,0x23,0x34);
    xStyleLvgl.body.main_color = xStyleLvgl.body.grad_color = LV_COLOR_MAKE(0x21,0x29,0x31);
    vRegisterDefaultStyle(&xStyleLvgl);
    vUpdateScreen(eWindowLogo);

}

int iAllocateObjects(CoreArgs_t* pxPrvArgs, uint16_t number)
{
    pxPrvArgs->usObjNumber = number;
    pxPrvArgs->apxObjects = (lv_obj_t**)malloc(sizeof(lv_obj_t*)*pxPrvArgs->usObjNumber);
    if(!pxPrvArgs->apxObjects)
    {
        return -1;
    }
    return 0;
}


/**********************
 *   STATIC FUNCTIONS
 **********************/

static int _iScreenIcon(CoreArgs_t* pxPrvArgs, void* pvArgs)
{
    iAllocateObjects(pxPrvArgs, 2);

    //pxPrvArgs->apxObjects[0] = lv_img_create(lv_scr_act(), NULL);
    pxPrvArgs->apxObjects[1] =  lv_label_create(lv_scr_act(), NULL);

    /* Set the image's file according to the current color depth
     * a blue flower picture*/
    //lv_img_set_src( pxPrvArgs->apxObjects[0], "P:/logoCS.bin");

    //lv_obj_align(img_bin, img_src, LV_ALIGN_OUT_RIGHT_TOP, 20, 0);     /*Align next to the s'ource image'*/
    //lv_obj_set_click(img_bin, true);
    //lv_obj_set_drag( pxPrvArgs->apxObjects[0], true);
    //lv_obj_set_pos(img_bin, 0, 0);

    _pxBattery = pxCreateBattery(lv_scr_act());
    lv_obj_align(_pxBattery, lv_scr_act(), LV_ALIGN_IN_TOP_RIGHT, 0, 0);

    _pxInfoWidget = pxCreateInfoWidget(lv_scr_act(), 350);
    lv_obj_align(_pxInfoWidget, lv_scr_act(), LV_ALIGN_IN_TOP_LEFT, 0, 0);
    vSetRangeInfoWidget(_pxInfoWidget, -50, 50);

    //version text
    lv_label_set_text(pxPrvArgs->apxObjects[1], "1.0");
    lv_obj_align(pxPrvArgs->apxObjects[1], lv_scr_act(), LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);

    return 0;
}
static int _iUpdateLogo(CoreArgs_t* pxPrvArgs, void* pvArgs)
{
	static int i = 0;
	i++;
	if (i <= 300) {
		vSetColorInfoWidget(_pxInfoWidget, LV_COLOR_MAKE((i * 2) % 0xFF, 0xFF - ((i * 2) % 0xFF), (i) % 0xFF));
		vSetVolumeInfoWidget(_pxInfoWidget, (float)i * 100 / 1000);
		vSetHoursInfoWidget(_pxInfoWidget, (float)i * 10 / 1000);
		vSetFlowInfoWidget(_pxInfoWidget, (float)i * 10 / 1000);
		vSetTempInfoWidget(_pxInfoWidget, (float)i * 10 / 1000);
		vSetPressureInfoWidget(_pxInfoWidget, (float)i * 1000 / 1000);
		vSetHumidityInfoWidget(_pxInfoWidget, (float)i * 10 / 1000);

		vSetMinusFlowInfoWidget(_pxInfoWidget, -(float)i * 11.23 / 1000);
		vSetPlusFlowInfoWidget(_pxInfoWidget, (float)i * 8 / 10000);

		vSetMinusTempInfoWidget(_pxInfoWidget, -(float)i * 7 / 1000);
		vSetPlusTempInfoWidget(_pxInfoWidget, (float)i * 11 / 10000);

		vSetMinusPressureInfoWidget(_pxInfoWidget, -(float)i * 10.236 / 10000);
		vSetPlusPressureInfoWidget(_pxInfoWidget, (float)i * 6.28 / 10000);

		vSetMinusHumidityInfoWidget(_pxInfoWidget, -(float)i * 5.5499 / 10000);
		vSetPlusHumidityInfoWidget(_pxInfoWidget, (float)i * 6.23 / 1000);
	    vSetHeaderNumberInfoWidget(_pxInfoWidget, i % 100 + 1);
		vSetFooterNumberInfoWidget(_pxInfoWidget, i % 10 + 1, i % 100 + 1);

		vAddDataInfoWidget(_pxInfoWidget, sin((float)i / 10) * 25);
	}

	printf("i: %d\n", i);
	usleep(20000); // microseconds

	return 0;
}
