/** @copyright Copyright (C) C&S Company, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 * @author    MARAZANO Lilian (l.marazano@cs-equipments.com)
 * @date      2019
 */
/**
 *  @file pump.h
 *  @brief
 *  @pre
 *  @details
 */

#ifndef PUMP_H_
#define PUMP_H_

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "lvgl/lvgl.h"


/*********************
 *      DEFINES
 *********************/
#define STATE_TABLE(ENTRY)            \
        ENTRY(eWindowLogo, _iScreenIcon, _iUpdateLogo) \
        ENTRY(eWindowIntro, NULL, NULL) \

#if(BUTTON_EXTERN)
#define coreGENERATE_INDEX_ENUM(a,b,c,d) a,
#define coreGENERATE_WIN_ARRAY(a,b,c,d) { .pcName = #a , .xFnStatic = b, .xFnUpdate = c, .xFnButton = d },
#else
#define coreGENERATE_INDEX_ENUM(a,b,c) a,
#define coreGENERATE_WIN_ARRAY(a,b,c) { .pcName = #a , .xFnStatic = b, .xFnUpdate = c },
#endif
/**********************
 *     TYPEDEFS
 **********************/
/* declare an enumeration of state codes */
enum
{
  STATE_TABLE(coreGENERATE_INDEX_ENUM)
};
typedef uint8_t WindowsName_e;

//TODO: add bool clean variable for multiple windows on screen
typedef struct {
    uint16_t usObjNumber;
    lv_obj_t** apxObjects;
} CoreArgs_t;

typedef int (*iCoreWindowsCb_t)(CoreArgs_t* ppxPrvArgs, void* pvArgs);
#if (1)
typedef void (*vCoreButtonb_t)(uint8_t ucAction, void* pvArgs);
#endif


//Structural screen system
typedef struct {
    const char* pcName;
    iCoreWindowsCb_t xFnStatic;
    iCoreWindowsCb_t xFnUpdate;
#if (BUTTON_EXTERN)
    vCoreButtonb_t xFnButton;
#endif
} CoreWindowInfos_t;

/**********************
 *  GLOBAL MACROS
 **********************/


/**********************
 *  GLOBAL VARIABLES
 **********************/


/**********************
 *  GLOBAL PROTOTYPES
 **********************/
void InitScreen(void);

#endif /* PUMP_H_ */
