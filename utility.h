/** @copyright Copyright (C) C&S Company, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 * @author    MARAZANO Lilian (l.marazano@cs-equipments.com)
 * @date      2019
 */
/**
 *  @file pump.h
 *  @brief
 *  @pre
 *  @details
 */


#ifndef UTILITY_H_
#define UTILITY_H_

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "lvgl/lvgl.h"

/*********************
 *      DEFINES
 *********************/


/**********************
 *     TYPEDEFS
 **********************/

/**********************
 *  GLOBAL MACROS
 **********************/

/**********************
 *  GLOBAL VARIABLES
 **********************/


/**********************
 *  GLOBAL PROTOTYPES
 **********************/
void vRegistrationScreen(const CoreWindowInfos_t* axGuiWindow, uint16_t usSize);
void vRegisterDefaultStyle(lv_style_t* pxStyle);
void vUpdateScreen(uint16_t usWdId);

#ifdef __cplusplus
}
#endif
#endif /* UTILITY_H_ */
